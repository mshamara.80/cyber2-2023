from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def hello_world():
    return """<a href='/login'>Login</a><br/> 
    <a href='/signup'>Signup</a>"""

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        return "<p>checking if ok</p>"
    elif request.method == "GET":
        return render_template("login.html")
    else:
        return "<p>Something went wrong</p>"

@app.route("/signup")
def signup():
    return "<p>Signup</p>"

@app.route("/welcome") 
def welcome():
    return "<p>welcome</p>"

@app.route("/welcome/<name>")
def welcome_name(name):
    return f"<p>welcome {name}</p>"



if __name__ == "__main__":
    app.run(debug=True) 